import Muse from './muse.js' ;

var statusMessage = document.getElementById("statusMessage") ;
var connectButton = document.getElementById("connectingButton") ;

const setStatusMessage = ( content, type ) => {
    statusMessage.innerText = content ;
    if (type === 'error')
        statusMessage.style.color = 'red' ;
    else if (type === 'warning')
        statusMessage.style.color = 'yellow' ;
    else
        statusMessage.style.color = 'green' ;
}

var muse = new Muse("Muse device", setStatusMessage) ;

// check if navigator supports bluetooth
console.log(navigator.bluetooth) ;
if (navigator.bluetooth === undefined)
{
    setStatusMessage('Your web browser does not handle web bluetooth. Please use Chrome or Chromium.', 'error') ;
    connectButton.disabled = true ;
}
else
{
    connectButton.addEventListener('click', () => {
        muse.connexion() ;
    })
}

