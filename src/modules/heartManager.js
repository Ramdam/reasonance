import { HeartManagerInfo, HeartManagerDeviceOptions } from './utils.mjs' ;

export default class HeartManager
{
    constructor(statusCallback, dataCallback)
    {
        this.primaryService = HeartManagerInfo.primaryService ;
        this.characteristic = HeartManagerInfo.characteristic ;
        this.hrmDeviceOptions = HeartManagerDeviceOptions ;
        this.currentDevice = undefined ;
        this.statusCallback = statusCallback ;
        this.dataCallback = dataCallback ;

        this.bufferLength = 60 ;
        this.smoothingCoefficient = 0.9 ;

        this.lastValue = 0 ;
        this.hr = 0 ;
        this.hrv = 0 ;
        this.hrBufferArray = Array(this.bufferLength).fill(120) ;
        this.hrvBufferArray = Array(this.bufferLength).fill(1000) ;

        this.metrics = {
            raw : {
                'hr' : 0,
                'hrv' : 0,
            },
            normalized : {
                'hr' : 0,
                'hrv' : 0,
            },
            smoothed : {
                'hr' : 0,
                'hrv' : 0,
            }
        }
    }

    ClampData = (data, min, max) => {
        if (data < min)
            data = min ;
        if (data > max)
            data = max ;
        return data ;
    }

    SendData = () => {
        setInterval(() => {
            this.dataCallback({
                metrics : this.metrics
            })
        }, 1000) ;
    }

    ComputeHRV = () => {

        let _ibis = [] ;
        let _ssd = 0 ;
        
        this.hrBufferArray.forEach( (value) => {
            _ibis.push(1000.0 / value) ;
        }) ;

        _ibis.forEach( (value, index) => {
        if (index < _ibis.length - 1)
            _ssd += Math.pow( (_ibis[index] - _ibis[index+1]), 2) ;
        }) ;

        return Math.sqrt(_ssd/(this.hrBufferArray.length - 1)) ;
    }

    ComputeMetrics = () => {

        this.hr = this.lastValue ;
        this.hrBufferArray.push(this.lastValue) ;
        this.hrBufferArray = this.hrBufferArray.slice(1) ;

        this.hrv = this.ComputeHRV() ;
        this.hrvBufferArray.push(this.hrv) ;
        this.hrvBufferArray = this.hrvBufferArray.slice(1) ;

        let sortedHR = [...this.hrBufferArray].sort() ;
        let sortedHRV = [...this.hrvBufferArray].sort() ;

        let _min = sortedHR[5] ;
        let _max = sortedHR[54] ;
        let nHR = 0 ;
        if (_max > _min)
        {
            nHR = this.ClampData((this.hr - _min) / (_max - _min), 0, 1) ;
        }

        _min = sortedHRV[5] ;
        _max = sortedHRV[54] ;
        let nHRV = 0 ;
        if (_max > _min)
        {
            nHRV = this.ClampData((this.hrv - _min) / (_max - _min), 0, 1) ;
        }

        let sHR = this.smoothingCoefficient * this.metrics.smoothed.hr + ( 1 - this.smoothingCoefficient) * nHR ;      
        let sHRV = this.smoothingCoefficient * this.metrics.smoothed.hrv + ( 1 - this.smoothingCoefficient) * nHRV ;      

        this.metrics = {
            raw : {
                'hr' : this.hr,
                'hrv' : this.hrv
            },
            normalized : {
                'hr' : nHR,
                'hrv' : nHRV
            },
            smoothed : {
                'hr' : sHR,
                'hrv' : sHRV
            }
        }        
    }


    handle = (event) => {
        let value = event.target.value ;
        this.lastValue = value.getUint8(1) ;
        this.ComputeMetrics() ;
    }

    async connexion()
    {
        var connected = false ;

        await navigator.bluetooth.requestDevice(this.hrmDeviceOptions)
            .then(function (device) {
                return device.gatt.connect();
            })
            .then(server => {
                return server.getPrimaryService(this.primaryService)
            })
            .then(service => {
                return service.getCharacteristic(this.characteristic) ;
            })
            .then(characteristic => characteristic.startNotifications())
            .then(characteristic => {
                characteristic.addEventListener('characteristicvaluechanged', this.handle) ;
                this.SendData() ;
                this.statusCallback(true) ;
            })
            .catch(function (error) {
                console.log("Something went wrong. " + error);
                connected = false
            }) ;
        return connected
    }

}
