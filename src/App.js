import Splashscreen from "./screens/splashscreen.js" ;

import PrivateRoute from "./functions/PrivateRoute" ;
import { Route, Routes } from 'react-router' ;

import GamepadSync from "./gamescreens/gamepadsync.js" ;
import BrainSolo from "./gamescreens/brainsolo.js" ;
import BrainDyad from "./gamescreens/brainsync.js" ;
import BreathingDyad from "./gamescreens/breathingsync.js" ;

const App = () => {

	return (
      <Routes>
        <Route path = '/' element = { <Splashscreen/> } exact/>
        <Route path = '/gamepadsync' element = { <GamepadSync/> } exact/>
        <Route path = '/breathingsync' element = { <BreathingDyad/> } exact/>
        <Route path = '/brainsolo' element = { <BrainSolo/> } exact/>
        <Route path = '/brainsync' element = { <BrainDyad/> } exact/>

      </Routes>
  ) ;
}

export default App ;