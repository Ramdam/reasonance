import { useState, useEffect } from 'react' ;

import { Grid, Box, Typography, Button, Stack, List, Card, CardMedia, CardContent, CardActions } from '@mui/material' ;
import { makeStyles, useTheme } from '@mui/styles' ;

import { useFirebase, isEmpty, isLoaded } from 'react-redux-firebase' ;
import { useSelector } from "react-redux" ;
import { useNavigate } from 'react-router' ;

import { onAuthChanged } from '../functions/firestore_funcs.js' ;

import AppMenu from '../components/appmenu.js' ;
import LoginDialog from '../components/logindialog.js' ;
import RegisterDialog from '../components/registerdialog' ;

import GameCard from '../components/gamecard.js' ;
import { games } from '../assets/cards/dicts/games.js' ;

const useStyles = makeStyles((theme) => ({

    mainContainer : {
        width : '100vw',
        minHeight : '100vh',
        display : 'flex',
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
        background : theme.palette.background.primary
    },  
    containerGrid : {
        marginTop : '2vh',
        height : '100%',
    },
    title : {
        height : '10%',
        display : 'flex',
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
        '& > h1': {
            fontFamily : 'Bebas Neue',
            fontSize : '6.4rem',
        }
    },
    description : {
        height : '30%',
        display : 'flex',
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
        '& > h4': {
            fontStyle : 'italic',
            fontSize : '2.4rem',
        }
    },
    actions : {
        width : '100vw',
        paddingTop : '40px',
        paddingBottom : '40px',
    },
    action : {
        width : '240px',
        height : '160px',
        background : theme.palette.button.background,
    },
}))


const Splashscreen = ( { navigation } ) => {

    const theme = useTheme() ;
    const classes = useStyles(theme) ;

    const navigate = useNavigate() ;

    const firebase = useFirebase() ;
    const auth = useSelector(state => state.firebase.auth) ;

    const [ isLogged, setIsLogged ] = useState(false) ;
    const [ isRegisterDialogOpen, setIsRegisterDialogOpen ] = useState(false) ;
    const [ isLoginDialogOpen, setIsLoginDialogOpen ] = useState(false) ;

    const handleRegisterDialogChange = value => {
        setIsRegisterDialogOpen(value) ;
        if (value === false)
            onAuthChanged(firebase) ;

    }
    const handleLoginDialogChange = value => {
        setIsLoginDialogOpen(value) ;
    }

    useEffect( () => {
    
    }, [])

    useEffect(() => {
        if (!isEmpty(auth) && isLoaded(auth))
        {
            setIsLogged(true) ;
            setTimeout(() => {
                onAuthChanged(firebase) ;
    
            }, 2000)
        }
        else
            setIsLogged(false) ;
    }, [ auth ])

    useEffect( () => {
    }, [ isLogged ])

    return (
        <Box className = { classes.mainContainer } >

            <AppMenu hidden = { false } />
 
            <Grid container direction = 'row' alignItems = 'center' justifyContent = 'center' className = { classes.containerGrid }>
                <Grid item xs = { 12 } md = { 12 } lg = { 10 } className = { classes.title }>
                    <Typography variant = 'h1' color = 'primary' style = {{ fontFamily : 'EnsoRegular'}} >REASONANCE</Typography>
                </Grid>
                <Grid item xs = { 12 } md = { 12 } lg = { 7 } className = { classes.description }>
                    <Typography variant = 'h4' color = 'secondary' align = 'center' style = {{ fontSize : 42, fontFamily : 'SwanseBold' }}>Using physiological symbiosis mechanism for a better mental health</Typography>
                </Grid>

                <Grid container alignItems = 'center' justifyContent = 'center' spacing = { 4 } className = { classes.actions }>
                    {
                        isLogged ?
                            games.map( item => {
                                console.log(item)
                                return (
                                    <Grid item key = { item.title }>
                                        <GameCard
                                            imgUrl = { item.background }
                                            title = { item.title }
                                            description = { item.description }
                                            onSelect = {() => { navigate(item.screen) }}
                                        />
                                    </Grid>
                                )
                            })
                            :
                            <div style = {{ width : '80%', display : 'flex', flexDirection : 'row', alignItems : 'center', justifyContent : 'center', flexWrap : 'wrap' }} >
                                <RegisterDialog status = { isRegisterDialogOpen } onClose = { () => handleRegisterDialogChange(false) } >
                                    <Button sx = {{ margin : '10px 120px', fontSize : '24px', borderRadius : 2,}} className = { classes.action } variant = "contained" onClick = { () => { handleRegisterDialogChange(true) } } >REGISTER</Button>
                                </RegisterDialog>

                                <LoginDialog status = { isLoginDialogOpen} onClose = { () => handleLoginDialogChange(false) }>
                                    <Button sx = {{ margin : '10px 120px', fontSize : '24px', borderRadius : 2,}} className = { classes.action } variant = "contained" onClick = { () => { handleLoginDialogChange(true) } } >LOGIN</Button>
                                </LoginDialog>
                            </div>
                    }
                </Grid>
            </Grid>
        </Box>
    )
}

export default Splashscreen ;