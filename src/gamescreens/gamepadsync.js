import { useState, useEffect } from 'react' ;

import { Grid, Box, Typography, } from '@mui/material' ;
import { makeStyles, useTheme } from '@mui/styles' ;

import { useNavigate } from 'react-router' ;

import MenuGamepadDyad from '../components/menugamepaddyad.js' ;

const useStyles = makeStyles((theme) => ({

    mainContainer : {
        width : '100vw',
        minHeight : '100vh',
        display : 'flex',
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
        background : theme.palette.background.primary
    },  
    containerGrid : {
        marginTop : '2vh',
        height : '100%',
    },

}))




const GamepadSync = () => {

    const theme = useTheme() ;
    const classes = useStyles(theme) ;

    useEffect( () => {
    
    }, [])

    return (
        <Box className = { classes.mainContainer } >
 
            <Grid container direction = 'row' alignItems = 'center' justifyContent = 'center' className = { classes.containerGrid }>
            </Grid>

            <MenuGamepadDyad
                onDataChange = { ( data ) => console.log(data) }
                hidden = { true }
            />

        </Box>
    )
    
}

export default GamepadSync ;