import { useSelector } from 'react-redux' ;
import { isLoaded, isEmpty } from 'react-redux-firebase' ;
import { Navigate } from 'react-router-dom' ;

const PrivateRoute = ({ children }) => {
	const auth = useSelector(state => state.firebase.auth)
    return isLoaded(auth) && !isEmpty(auth) ? children : <Navigate to = "/" /> ;
}

export default PrivateRoute ;
