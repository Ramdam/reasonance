export const onAuthChanged = (firebase) => {

    console.log(firebase.auth().currentUser.displayName) ;

    const username = firebase.auth().currentUser.displayName ;
    const uid = firebase.auth().currentUser.uid ;
    const email = firebase.auth().currentUser.email ;
    
    if (uid !== undefined)
    {
        firebase.firestore().collection('Users').doc(uid).get()
        .then((docSnapshot) => {
            if (docSnapshot.exists) {
                firebase.firestore().collection('Users').doc(uid).update(
                    {
                        timestamp : new Date(),
                    }
                ) ;
            }
            else
            {
                firebase.firestore().collection('Dicts').doc('Users').get()
                .then( dictSnapshot => {
                    if (dictSnapshot.exists) {
                        firebase.firestore().collection('Dicts').doc('Users').update(
                            {                            
                                emails : firebase.firestore.FieldValue.arrayUnion(email),
                                uids : firebase.firestore.FieldValue.arrayUnion(uid),
                                members : firebase.firestore.FieldValue.arrayUnion({
                                    username : username,
                                    email : email,
                                    uid : uid,
                                })
                            }
                        )
                    }
                    else
                    {
                        firebase.firestore().collection('Dicts').doc('Users').set(
                            {                            
                                emails : firebase.firestore.FieldValue.arrayUnion(email),
                                uids : firebase.firestore.FieldValue.arrayUnion(uid),
                                members : firebase.firestore.FieldValue.arrayUnion({
                                    username : username,
                                    email : email,
                                    uid : uid,
                                })
                            }
                        )
                    }
                }) ;

                firebase.firestore().collection('Users').doc(uid).set(
                    {
                        timestamp : new Date(),
                        metadata : {
                            createdAt : new Date(),
                            username : username,
                            email : email,
                        },
                        state : {
                        },
                        tribesasadmin : [],
                        tribesasmember : [],
                        tribeSelected : '',
                    }
                )
            }
        });
    }
}