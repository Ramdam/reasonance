import React from 'react' ;
import ReactDOM from 'react-dom' ;
import App from './App' ;
import firebase from "firebase/compat/app" ;
import 'firebase/compat/auth' ;
import 'firebase/compat/firestore' ;
import 'firebase/compat/database' ;
import { Provider } from 'react-redux' ;
import { createStore } from 'redux' ;
import { ReactReduxFirebaseProvider } from 'react-redux-firebase' ;
import { createFirestoreInstance } from "redux-firestore" ;
import { rootReducer } from './redux/reducers.js' ;
import { BrowserRouter } from 'react-router-dom' ;
import { firebaseConfig } from './config/firestore_inf' ;
import { createTheme, ThemeProvider } from '@mui/material/styles' ;

import './index.css' ;
import './assets/fonts/Enso-regular.ttf' ;
import './assets/fonts/Swanse-regular.ttf' ;
import './assets/fonts/Swanse-bold.ttf' ;
import './assets/fonts/Swanse-alternate.ttf' ;
import './assets/fonts/Swanse-alternatebold.ttf' ;

const theme = createTheme({
	palette : {
	  	primary : {
			main : '#ff0a85',
	  	},
		secondary : {
			main : '#3b3b3b',
	  	},
	  	white : {
			main : '#ffffff',
	  	},
		red : {
			main : '#ff0000',
	  	},  
	  	disconnected : {
			main : '#cc6666',
		},
		connected : {
			main : '#339933',
		},
		background : {
			primary : '#fff4e6',
			secondary : '#9adcff',
			gradient : 'linear-gradient(to bottom, #fff4e6, #fff4e6)',
		},
		menu : {
			gradient : 'linear-gradient(to bottom, #fff4e6, #f5dadf)',
			iconcolor : '#ffffff',
			transparent : '#ffffff00',
		},
		text : {
			primary : '#ff8aae',
			secondary : '#9adcff6',
			white : '#ffffff'
		},
		typography: {
			subtitle1: {
			},
			h1: {
			},
			body1: {
				color : '#ff8aae',
			},
			button: {
				color : '#ffffff',
				fontSize: 64,
			},
		},
		button : {
			background : '#ffffff',
			first : 'linear-gradient(to right, #fbf46d33 0%, #fbf46dff 51%, #fbf46d33 100%)',
			second : 'linear-gradient(to right, #1a2980 0%, #26d0ce 51%, #1a2980 100%)',
			third : 'linear-gradient(to right, #ac2f26 0%, #f3a183 51%, #ac2f26 100%)',
			fourth : 'linear-gradient(to right, #233329 0%, #63d471 51%, #233329 100%)',
		},
	},
	components: {
		MuiButton: {
			styleOverrides: {
				contained: {
					color: 'white',
				},
			},
		},
	  }	
});

firebase.initializeApp(firebaseConfig) ;
firebase.firestore() ;

const rrfConfig = {
	userProfile: "Users",
	useFirestoreForProfile: true,
} ;

const initialState = {} ;
const store = createStore(rootReducer, initialState) ;

const rrfProps = {
	firebase,
	config: rrfConfig,
	dispatch: store.dispatch,
	createFirestoreInstance,
};

ReactDOM.render(
	<React.StrictMode>
		<ThemeProvider theme = {theme}>
			<Provider store = {store}>
				<ReactReduxFirebaseProvider {...rrfProps}>
					<BrowserRouter>
						<App />
					</BrowserRouter>
				</ReactReduxFirebaseProvider>
			</Provider>
		</ThemeProvider>
	</React.StrictMode>,
	document.getElementById('root')
) ;