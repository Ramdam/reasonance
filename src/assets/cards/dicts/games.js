export const games = [

    {
        background : require('../backgrounds/01.png'),
        title : 'Treasure Hunter',
        description : 'Using you gamepad, your treasure hunter will move accordingly to both directions.',
        color : 'blue',
        tags : ['dyad', 'gamepad'],
        screen : 'gamepadsync',
    },
    {
        background : require('../backgrounds/01.png'),
        title : 'Flying monks',
        description : 'this is a game about that, and that, and also that',
        color : 'blue',
        tags : ['dyad', 'brain'],
        screen : 'brainsync',
    },
    {
        background : require('../backgrounds/01.png'),
        title : 'Mandala',
        description : 'this is a game about that, and that, and also that',
        color : 'blue',
        tags : ['solo', 'brain'],
        screen : 'brainsolo',
    },
    {
        background : require('../backgrounds/01.png'),
        title : 'Apneids',
        description : 'this is a game about that, and that, and also that',
        color : 'blue',
        tags : ['dyad', 'breathing'],
        screen : 'breathingsync',
    },
    {
        background : require('../backgrounds/01.png'),
        title : 'game05',
        description : 'this is a game about that, and that, and also that',
        color : 'blue',
        tags : ['dyad', 'breathing'],
        screen : 'dyad',
    }

]