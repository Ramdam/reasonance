import { useState, useEffect } from 'react' ;
import { Typography, TextField, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material' ;
import { useFirebase } from 'react-redux-firebase' ;
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";

import DefaultAvatar from '../assets/sprites/defaultAvatar.png' ;

const RegisterDialog = ( { status, onClose, children } ) => {

    const firebase = useFirebase() ;
    const storage = getStorage() ;

    const [ email, setEmail ] = useState('') ;
    const [ emailError, setEmailError ] = useState(false) ;
    const [ password, setPassword ] = useState('') ;
    const [ passwordError, setPasswordError ] = useState(false) ;
    const [ username, setUsername ] = useState('') ;
    const [ usernameError, setUsernameError ] = useState(false) ;
    const [ errorMessage, setErrorMessage ] = useState('') ;

    const allInputs = { imgUrl : DefaultAvatar } ;
    const [ imageAsFile, setImageAsFile ] = useState('') ;
    const [ imageAsUrl, setImageAsUrl ] = useState(allInputs) ;

    const handleImageAsFile = (e) => {
        const image = e.target.files[0] ;
        setImageAsFile(imageFile => (image)) ;
        setImageAsUrl({
            imgUrl : URL.createObjectURL(image)
        })
    }

    const [ dialogRegisterOpen, setDialogRegisterOpen ] = useState(false) ;
    const handleRegisterOpen = () => {
        setDialogRegisterOpen(true) ;
    }
    const handleRegisterClose = () => {
        setDialogRegisterOpen(false) ;
        onClose(false) ;
    }

    useEffect( () => {
        if (status === true)
        {
            handleRegisterOpen() ;
        }
    }, [ status ])

    const handleFieldChange = (event) => {
        if (event.target.name === 'email')
        {
            setEmailError(false) ;
            setEmail(event.target.value) ;
        }
        else if (event.target.name === 'password')
        {
            setPasswordError(false) ;
            setPassword(event.target.value) ;
        }
        else if (event.target.name === 'username')
        {
            setUsernameError(false) ;
            setUsername(event.target.value) ;
        }
    }

    const handleSubmitRegister = (event) => {
        event.preventDefault();
        setErrorMessage('') ;
        if (username === '') {
            setUsernameError(true) ;
        }
        if (email === '') {
          setEmailError(true) ;
        }
        if (password === '') {
          setPasswordError(true) ;
        }
        if (username !== '' && password !== '' && email !== '') {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then( () => {
                const user = firebase.auth().currentUser ;
                user.updateProfile({
                    displayName : username,
                })
                .then( () => {
                    handleRegisterClose() ;
                })
            })
            .catch((error) => {
                setErrorMessage(error.message);
            }) ;
        }
    }

    return (
        <div>
            <Dialog open = { dialogRegisterOpen } onClose = { handleRegisterClose }>
                <DialogTitle>Register</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        To create a new account, please enter your email address, your password and your username. You also can add a photo id.
                    </DialogContentText>

                    <div style = {{ margin : 40 }}></div>

                    {/*
                    <div style = {{ display : 'flex', flexDirection : 'row', justifyContent : 'center', alignItems : 'center'}}>

                        <img src = { imageAsUrl.imgUrl } width = {92} style = {{ borderRadius : '50%', marginRight : 50 }}></img>

                        <label htmlFor = "upload-photo">
                            <input
                                style = {{ display: 'none' }}
                                id = "upload-photo"
                                name = "upload-photo"
                                type = "file"
                                onChange = { handleImageAsFile }
                                accept = "image/png, image/jpg"
                            />

                            <Button color = "primary" variant = "contained" component = "span">
                                Change your avatar
                            </Button>
                        </label>
                    
                    </div>

                    <div style = {{ margin : 20 }}></div>
                    */}
                    <TextField
                        margin = "dense"
                        id = "username"
                        name = "username"
                        label = "Username"
                        type = "username"
                        fullWidth
                        required
                        value = { username }
                        onChange = { handleFieldChange }
                        autoComplete = "username"
                        variant = "standard"
                        error = { usernameError }
                    />
                    <TextField
                        margin = "dense"
                        id = "name"
                        name = "email"
                        label = "Email Address"
                        type = "email"
                        fullWidth
                        required
                        value = { email }
                        onChange = { handleFieldChange }
                        autoComplete = "email"
                        variant = "standard"
                        error = { emailError }
                    />
                    <TextField
                        margin = "dense"
                        id = "password"
                        name = "password"
                        label = "Password"
                        type = "password"
                        secured = "true"
                        fullWidth
                        required
                        value = { password }
                        onChange = { handleFieldChange }
                        autoComplete = "password"
                        variant = "standard"
                        error = { passwordError }
                    />
                    {
                        errorMessage ? (
                            <Typography color = "error">{errorMessage}</Typography>
                        ) : null
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick = { handleRegisterClose }>Cancel</Button>
                    <Button autoFocus onClick = { handleSubmitRegister }>Register</Button>
                </DialogActions>
            </Dialog>
            { children }
        </div>
    )
}


export default RegisterDialog ;