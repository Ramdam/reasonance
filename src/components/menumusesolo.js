import { useState, useEffect } from 'react' ;
import { Box, Typography, IconButton, AppBar, Toolbar, List, Button, Select, } from '@mui/material' ;
import { makeStyles, useTheme } from '@mui/styles' ;
import Fade from '@mui/material/Fade' ;
import MenuRoundedIcon from '@mui/icons-material/MenuRounded' ;

import { useNavigate } from 'react-router' ;

const useStyles = makeStyles((theme) => ({

    containerBox : {
        position : 'fixed',
        bottom : '2px',
        width : '95%',
        margin : '20px 0px',
        backgroundColor : theme.palette.background.primary,
    },

    appBar : {
        backgroundColor : theme.palette.background.primary,
        transition : '5s ease'
    },
    menuBurger : {
        position : 'fixed',
        bottom : '20px',
        right : '20px',
        transition : '5s ease'
    },
}))

const MenuMuseSolo = ( { hidden, onDataChange } ) => {

    const theme = useTheme() ;
    const classes = useStyles(theme) ;
    
    const [ isHidden, setIsHidden ] = useState( (hidden === undefined) ? true : hidden) ;

    const navigate = useNavigate() ;

    return (
        <>
            <Box className = {classes.menuBurger} style = {{ display : isHidden ? 'block' : 'none'}} >
                <IconButton sx = {{ float : 'right'}} color = "primary" onClick = { () => { setIsHidden(false ) } }>
                    <MenuRoundedIcon />
                </IconButton>
            </Box>

            <Box className = { classes.containerBox } sx = {{display : !isHidden ? 'block' : 'none'}}>
                <AppBar style = {{ backgroundColor : theme.palette.background.primary }} elevation = { 0 } sx = {{ borderTop : "1px #dedede solid"  }} position = "static" className = { classes.appBar }>
                    <Toolbar style = {{ display : 'flex', flexDirection : 'row', justifyContent : 'space-evenly' }}>

                    </Toolbar>
                </AppBar>
            </Box>
        </>

    )
}



export default MenuMuseSolo ;