import { Typography, Button, Card, CardMedia, CardContent, CardActions } from '@mui/material' ;
import { makeStyles, useTheme } from '@mui/styles' ;


const GameCard = ({ imgUrl, title, description, onSelect }) => {

    const theme = useTheme() ;
    console.log(imgUrl) ;

    return(
        <Card elevation = { 4 } sx = {{ padding : 2, width: 345, height : 240, background : theme.palette.background.gradient }}>
            <CardMedia
                component = "img"
                alt = "green iguana"
                height = "100"
                image = { imgUrl }
            />
            <CardContent>
                <Typography gutterBottom variant = "h5" component = "div" color = 'secondary'>
                    {title}
                </Typography>
                <Typography variant = "body2" color = "secondary">
                    {description}
                </Typography>
            </CardContent>
            <CardActions style = {{ display : 'flex', flexDirection : 'row', justifyContent : 'flex-end', alignItems : 'flex-end'}} >
                <Button size = "small" variant = "outlined" onClick = { () => onSelect() } >Play</Button>
            </CardActions>
        </Card>
    )
}



export default GameCard ;