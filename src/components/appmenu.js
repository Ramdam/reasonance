import { useState, useEffect } from 'react' ;

import { Box, Typography, Tooltip, AppBar, Toolbar, List, Badge, Button, IconButton, Menu, MenuItem, Dialog, DialogActions, FormControl, InputLabel, Select, DialogContent, DialogContentText, DialogTitle } from '@mui/material' ;
import Fade from '@mui/material/Fade' ;
import { makeStyles, useTheme } from '@mui/styles' ;

import Image from 'mui-image' ;
import LogoWorld from '../assets/logos/logo_world.png' ;

import AccountCircleRoundedIcon from '@mui/icons-material/AccountCircleRounded' ;
import GroupsRoundedIcon from '@mui/icons-material/GroupsRounded' ;
import MailOutlineRoundedIcon from '@mui/icons-material/MailOutlineRounded' ;
import MenuRoundedIcon from '@mui/icons-material/MenuRounded' ;
import ClearRoundedIcon from '@mui/icons-material/ClearRounded' ;

import { getFirestore, collection, doc, addDoc, setDoc, updateDoc, getDoc, query, where, limit, getDocs, onSnapshot, serverTimestamp, arrayUnion, arrayRemove, increment, deleteDoc } from "firebase/firestore" ;
import { useFirebase, isEmpty, isLoaded } from 'react-redux-firebase' ;
import { useSelector } from "react-redux" ;

import { useNavigate } from 'react-router' ;

import LoginDialog from '../components/logindialog.js' ;
import RegisterDialog from '../components/registerdialog' ;
import CreateTribeDialog from '../components/createtribedialog.js' ;
import JoinTribeDialog from '../components/jointribedialog.js' ;
import DeleteTribeDialog from '../components/deletetribedialog.js' ;
import LeaveTribeDialog from '../components/leavetribedialog.js' ;
import PendingRequestsDialog from '../components/pendingrequestsdialog.js' ;

const useStyles = makeStyles((theme) => ({

    containerBox : {
        width : '95%',
        margin : '20px 0px',
        backgroundColor : theme.palette.background.primary,
    },

    appBar : {
        backgroundColor : theme.palette.background.primary,
        transition : '5s ease'
    },
    menuBurger : {
        width : '90%',
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'flex-end',
        transition : '5s ease'
    },
}))

const AppMenu = ({ hidden }) => {

    const theme = useTheme() ;
    const classes = useStyles(theme) ;
    
    const [ isHidden, setIsHidden ] = useState( (hidden === undefined) ? true : hidden) ;

    const firebase = useFirebase() ;
    const auth = useSelector(state => state.firebase.auth) ;
    const [ isLogged, setIsLogged ] = useState(false) ;

    const db = getFirestore() ;

    const navigate = useNavigate() ;

    const [ uid, setUid ] = useState(undefined) ;
    const [ userDoc, setUserDoc ] = useState(undefined) ;
    const [ username, setUsername ] = useState(undefined) ;

    const [ tribes, setTribes ] = useState([])
    const [ tribesAsAdmin, setTribesAsAdmin ] = useState([])

    const [ pendingRequests, setPendingRequests ] = useState([]) ;
    const [ pendingMails, setPendingMails ] = useState([]) ;

    const [ menuAuthAnchorEl, setMenuAuthAnchorEl] = useState(null) ;
    const handleMenuAuthOpen = (event) => {
        setMenuAuthAnchorEl(event.currentTarget) ;
    } ;
    const handleMenuAuthClose = () => {
        setMenuAuthAnchorEl(null) ;
    } ;
    const [ menuTribeAnchorEl, setMenuTribeAnchorEl] = useState(null) ;
    const handleMenuTribeOpen = (event) => {
        setMenuTribeAnchorEl(event.currentTarget) ;
    } ;
    const handleMenuTribeClose = () => {
        setMenuTribeAnchorEl(null) ;
    } ;
    const [ menuMailboxAnchorEl, setMenuMailboxAnchorEl] = useState(null) ;
    const handleMenuMailboxOpen = (event) => {
        setMenuMailboxAnchorEl(event.currentTarget) ;
    } ;
    const handleMenuMailboxClose = () => {
        setMenuMailboxAnchorEl(null) ;
    } ;

    const [ isRegisterDialogOpen, setIsRegisterDialogOpen ] = useState(false) ;
    const handleRegisterDialogChange = value => {
        setIsRegisterDialogOpen(value) ;
    }

    const [ isLoginDialogOpen, setIsLoginDialogOpen ] = useState(false) ;
    const handleLoginDialogChange = value => {
        setIsLoginDialogOpen(value) ;
    }

    const [ isCreateTribeDialogOpen, setIsCreateTribeDialogOpen ] = useState(false) ;
    const handleCreateTribeDialogChange = value => {
        setIsCreateTribeDialogOpen(value) ;
    }

    const [ isJoinTribeDialogOpen, setIsJoinTribeDialogOpen ] = useState(false) ;
    const handleJoinTribeDialogChange = value => {
        setIsJoinTribeDialogOpen(value) ;
    }

    const [ isDeleteTribeDialogOpen, setIsDeleteTribeDialogOpen ] = useState(false) ;
    const handleDeleteTribeDialogChange = value => {
        setIsDeleteTribeDialogOpen(value) ;
    }

    const [ isLeaveTribeDialogOpen, setIsLeaveTribeDialogOpen ] = useState(false) ;
    const handleLeaveTribeDialogChange = value => {
        setIsLeaveTribeDialogOpen(value) ;
    }

    const [ isPendingRequestsDialogOpen, setIsPendingRequestsDialogOpen ] = useState(false) ;
    const handlePendingRequestsDialogChange = value => {
        setIsPendingRequestsDialogOpen(value) ;
    }

    useEffect(() => {
        if (!isEmpty(auth) && isLoaded(auth))
        {
            setIsLogged(true) ;
            setUid(auth.uid) ;    
        }
        else
        {
            setIsLogged(false) ;
            setUid(undefined) ;
            setUsername(undefined) ;
        }
    }, [ auth ])

    useEffect( () => {

        if (uid !== undefined)
        {
            setUserDoc(doc(db, "Users", uid)) ;
            FillPendingRequests() ;
            FillPendingMails() ;
        }

    }, [ uid ])

    useEffect(() => {
        if (userDoc !== undefined)
        {
            getDoc(userDoc)
            .then( docSnap => {
                setUsername(docSnap.data().metadata.username) ;
                FillTribes() ;
            })
        }
    }, [ userDoc ])

    const FillTribes = () => {
        if (userDoc !== undefined)
        {
            getDoc(userDoc)
            .then( docSnap => {
                if (docSnap.data().tribesasmember !== undefined)
                    setTribes(docSnap.data().tribesasmember) ;
                if (docSnap.data().tribesasadmin !== undefined)
                    setTribesAsAdmin(docSnap.data().tribesasadmin) ;
            })
        }
    }

    const FillPendingRequests = () => {

        getDocs(query(collection(db, 'Users/' + uid + '/Mailbox'), where('type', '==', 'joinrequest')))
        .then( docs => {
            let _requests = [] ;
            docs.forEach( ( item, index) => {
                _requests.push(
                    {
                        id : item.id,
                        data : item.data()
                    }
                )
            })
            setPendingRequests(_requests) ;
        })
    }

    const FillPendingMails = () => {

        getDocs(query(collection(db, 'Users/' + uid + '/Mailbox'), where('type', '==', 'message')))
        .then( docs => {
            let _mails = [] ;
            docs.forEach( ( item, index) => {
                _mails.push(
                    {
                        id : item.id,
                        data : item.data()
                    }
                )
            })
            setPendingMails(_mails) ;
        })
    }

    const SignOut = () => {
        firebase.auth().signOut() ;
    } ;

    return (
        <>
            <Box className = {classes.menuBurger} style = {{ display : isHidden ? 'block' : 'none'}} >
                <IconButton sx = {{ float : 'right'}} color = "primary" onClick = { () => { setIsHidden(false ) } }>
                    <Tooltip className = { classes.tooltip } title = "Open menu" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                        <MenuRoundedIcon />
                    </Tooltip>
                </IconButton>
            </Box>

            <Box className = { classes.containerBox } sx = {{display : !isHidden ? 'block' : 'none'}}>
                <AppBar style = {{ backgroundColor : theme.palette.background.primary }} elevation = { 0 } sx = {{ borderBottom : "1px #dedede solid"  }} position = "static" className = { classes.appBar }>
                    <Toolbar style = {{ display : 'flex', flexDirection : 'row', justifyContent : 'space-evenly' }}>

                        <div style = {{ width : '30%', }}>
                            <Image src = { LogoWorld } width = { 64 } height = { 64 } onClick = { () => {} } />
                        </div>
                        <div style = {{ width : '30%', display : 'flex', flexDirection : 'row', justifyContent : 'space-between', alignItems : 'center' }}>
                            <Button sx = {{ '&:hover' : { transform : 'scale(1.1)' } }} variant = "text" onClick={ () => navigate('/')}>Home</Button>
                            <Button sx = {{ '&:hover' : { transform : 'scale(1.1)' } }} variant = "text" onClick={ () => navigate('/solo')}>Solo</Button>
                            <Button sx = {{ '&:hover' : { transform : 'scale(1.1)' } }} variant = "text" onClick={ () => navigate('/dyad')}>Dyad</Button>
                            <Button sx = {{ '&:hover' : { transform : 'scale(1.1)' } }} variant = "text" onClick={ () => navigate('/tribe')}>Tribe</Button>
                            <Button sx = {{ '&:hover' : { transform : 'scale(1.1)' } }} variant = "text" onClick={ () => navigate('/')}>Sessions</Button>
                        </div>    

                        <div style = {{ width : '30%', display : 'flex', flexDirection : 'row'}}>

                            <div style = {{flexGrow : 1}}></div>

                            { isLogged &&
                                <Badge sx = {{ paddingLeft : '40px'}} badgeContent = { pendingRequests.length + pendingMails.length } color = "primary">
                                    <IconButton color = "primary" aria-label = "messages-management" component = "span" onClick = { handleMenuMailboxOpen }>
                                        <Tooltip className = { classes.tooltip } title = "Read messages and manage pending requests" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                            <MailOutlineRoundedIcon />
                                        </Tooltip>
                                    </IconButton>
                                </Badge>
                            }
                            
                            { isLogged && 
                                <IconButton sx = {{ paddingLeft : '40px'}} color = "primary" aria-label = "tribe-management" component = "span" onClick = { handleMenuTribeOpen }>
                                    <Tooltip className = { classes.tooltip } title = "Manage your tribes" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                        <GroupsRoundedIcon />
                                    </Tooltip>
                                </IconButton>
                            }
                            
                            <IconButton sx = {{ paddingLeft : '40px'}}color = "primary" aria-label = "account-management" component = "span" onClick = { handleMenuAuthOpen }>
                                <Tooltip className = { classes.tooltip } title = "Manage your account" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                    <AccountCircleRoundedIcon />
                                </Tooltip>
                            </IconButton>

                            <Menu
                                id = "menu-appbar"
                                anchorEl = { menuMailboxAnchorEl }
                                anchorOrigin = {{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin = {{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open = { Boolean(menuMailboxAnchorEl)}
                                onClose = { handleMenuMailboxClose }
                            >
                                <PendingRequestsDialog status = { isPendingRequestsDialogOpen } onClose = { () => { FillPendingRequests() ; handlePendingRequestsDialogChange(false) ; } } >
                                    <Tooltip className = { classes.tooltip } title = "Manage pending requests" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                        <MenuItem onClick = { () => { handleMenuMailboxClose() ; handlePendingRequestsDialogChange(true) ; }}>Pending requests</MenuItem>
                                    </Tooltip>
                                </PendingRequestsDialog>
                                <Tooltip className = { classes.tooltip } title = "Read messages" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                    <MenuItem onClick = { () => { handleMenuMailboxClose() ; }}>Mailbox</MenuItem>
                                </Tooltip>
                            </Menu>

                            <Menu
                                id = "menu-appbar"
                                anchorEl = { menuAuthAnchorEl }
                                anchorOrigin = {{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin = {{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open = { Boolean(menuAuthAnchorEl)}
                                onClose = { handleMenuAuthClose }
                            >
                                { !isLogged &&
                                    <LoginDialog status = { isLoginDialogOpen } onClose = { () => handleLoginDialogChange(false) } >
                                        <Tooltip className = { classes.tooltip } title = "Log into your account" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                            <MenuItem onClick = { () => { handleMenuAuthClose() ; handleLoginDialogChange(true) ; }}>
                                                Login
                                            </MenuItem>
                                        </Tooltip>
                                    </LoginDialog>
                                }
                                { !isLogged &&
                                    <RegisterDialog status = { isRegisterDialogOpen } onClose = { () => handleRegisterDialogChange(false) } >
                                        <Tooltip className = { classes.tooltip } title = "Create a new account" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                            <MenuItem onClick = { () => { handleMenuAuthClose() ; handleRegisterDialogChange(true) ; }}>
                                                Register
                                            </MenuItem>
                                        </Tooltip>
                                    </RegisterDialog>
                                }
                                { isLogged &&
                                    <Tooltip className = { classes.tooltip } title = "Manage your account" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                        <MenuItem onClick = { () => { handleMenuAuthClose() ; }}>
                                            Manage profile
                                        </MenuItem>
                                    </Tooltip>
                                }
                                { isLogged &&
                                    <Tooltip className = { classes.tooltip } title = "Log out of this account" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                        <MenuItem onClick = { () => { handleMenuAuthClose() ; SignOut() ; }}>
                                            Sign out
                                        </MenuItem>
                                    </Tooltip>
                                } 
                            </Menu>

                            { isLogged && 

                                <Menu
                                    id = "menu-appbar"
                                    anchorEl = { menuTribeAnchorEl }
                                    anchorOrigin = {{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin = {{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open = { Boolean(menuTribeAnchorEl)}
                                    onClose = { handleMenuTribeClose }
                                >
                                    <CreateTribeDialog status = { isCreateTribeDialogOpen } onClose = { () => { FillTribes() ; handleMenuTribeClose() ; handleCreateTribeDialogChange(false) ; } } >
                                    <Tooltip className = { classes.tooltip } title = "Create a new tribe" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                        <MenuItem onClick = { () => { handleCreateTribeDialogChange(true) ; }}>Create a tribe</MenuItem>
                                    </Tooltip>
                                    </CreateTribeDialog>                   
                                    <JoinTribeDialog status = { isJoinTribeDialogOpen } onClose = { () => { FillPendingRequests() ; handleMenuTribeClose() ; handleJoinTribeDialogChange(false) ; } } >
                                        <Tooltip className = { classes.tooltip } title = "Join an existing tribe" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                            <MenuItem onClick = { () => { handleJoinTribeDialogChange(true) ; }}>Join a tribe</MenuItem> 
                                        </Tooltip>
                                    </JoinTribeDialog>
                                    <LeaveTribeDialog status = { isLeaveTribeDialogOpen } onClose = { () => { FillTribes() ; FillPendingMails() ; handleMenuTribeClose() ; handleLeaveTribeDialogChange(false) ; }} >
                                        <Tooltip className = { classes.tooltip } title = "Leave one of your tribe" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                            <MenuItem onClick = { () => { handleLeaveTribeDialogChange(true) ; }}>Leave a tribe</MenuItem> 
                                        </Tooltip>
                                    </LeaveTribeDialog>
                                    <DeleteTribeDialog status = { isDeleteTribeDialogOpen } onClose = { () => { FillTribes() ; FillPendingMails() ; handleMenuTribeClose() ; handleDeleteTribeDialogChange(false) ; } } >
                                        <Tooltip className = { classes.tooltip } title = "Delete one of your tribe" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                            <MenuItem onClick = { () => { handleDeleteTribeDialogChange(true) ; }}>Delete a tribe</MenuItem>
                                        </Tooltip>
                                    </DeleteTribeDialog>
                                </Menu>
                            }

                            <div style = {{ flexGrow : .2 }}></div>

                            <IconButton sx = {{ paddingLeft : '40px'}} color = 'primary' aria-label = "account-management" component = "span" onClick = { () => { setIsHidden(true) } }>
                                <Tooltip className = { classes.tooltip } title = "Close menu" arrow TransitionComponent = { Fade } TransitionProps = {{ timeout: 600 }}>
                                    <ClearRoundedIcon />
                                </Tooltip>
                            </IconButton>
                        
                        </div>

                    </Toolbar>
                </AppBar>
            </Box>
        </>
    )
}

export default AppMenu ;