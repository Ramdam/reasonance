import React, { useRef, useEffect } from 'react'

const Canvas = props => {
  
  const canvasRef = useRef(null)
  
  useEffect(() => {
    const canvas = canvasRef.current ;
    const context = canvas.getContext('2d') ;
    //Our first draw
    context.fillStyle = '#000000' ;
    let img = new Image() ;
    img.setAttribute('crossOrigin', 'anonymous');

    img.onload = () =>
    {
        console.log(img.width, img.height)
        context.drawImage(img, props.x, props.y, img.width / 2, img.height / 2) ;    
    } ;
    img.src = '../../assets/sprites/littleBuddha.png' ;
  }, [])

  
  return <canvas ref = {canvasRef} {...props}/>
}

export default Canvas ;