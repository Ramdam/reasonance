import Box from '@mui/material/Box' ;
import Button from '@mui/material/Button' ;
import ListItem from '@mui/material/ListItem' ;
import ListItemButton from '@mui/material/ListItemButton' ;
import Divider from '@mui/material/Divider' ;
import ListItemText from '@mui/material/ListItemText' ;
import ListItemAvatar from '@mui/material/ListItemAvatar' ;
import Avatar from '@mui/material/Avatar' ;
import Typography from '@mui/material/Typography' ;

import defaultAvatar from '../assets/logos/logo_world.png' ;



const PendingRequestItem = ( { username, tribe, avatar, onAccept, onDecline } ) => {

    return (

        <ListItem alignItems = "flex-start" sx = {{ backgroundColor : '#ffffff', borderRadius : 5, border : '1px solid #efefef', marginTop : 2, marginBottom : 2, }}>

            <ListItemAvatar>
                <Avatar alt = "Remy Sharp" src = { avatar === undefined ? defaultAvatar : avatar } />
            </ListItemAvatar>
            <ListItemText
                primary = "Pending Request"
                secondary = { username + " would like to join your tribe " + tribe }
            />

            <Box sx = {{ height : '100%', display : 'flex', flexDirection : 'column', justifyContent : 'space-between'}}>
                <Button sx = {{ margin : 1, }} variant = 'contained' color = 'primary' size = "small" onClick = { () => onAccept() } >
                    Accept
                </Button>

                <Button sx = {{ margin : 1, }} variant = 'outlined' color = 'primary' size = "small" onClick = { () => onDecline() } >
                    Decline
                </Button>
            </Box>
        </ListItem>   
    )
}



export default PendingRequestItem ;