import { useState, useEffect } from 'react' ;
import { List, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material' ;
import { getFirestore, collection, doc, addDoc, setDoc, updateDoc, getDoc, query, where, limit, getDocs, onSnapshot, serverTimestamp, arrayUnion, arrayRemove, increment, deleteDoc } from "firebase/firestore" ;
import { useFirebase, isEmpty, isLoaded } from 'react-redux-firebase' ;
import { useSelector } from "react-redux" ;

import PendingRequestItem from './pendingrequestitem.js' ;

const PendingRequestsDialog = ( { status, onClose, children } ) => {

    const db = getFirestore() ;
    const firebase = useFirebase() ;
    const auth = useSelector(state => state.firebase.auth) ;
    const [ isLogged, setIsLogged ] = useState(false) ;

    const [ uid, setUid ] = useState(undefined) ;
    const [ userDoc, setUserDoc ] = useState(undefined) ;
    const [ username, setUsername ] = useState(undefined) ;

    useEffect(() => {
        if (!isEmpty(auth) && isLoaded(auth))
        {
            setIsLogged(true) ;
            setUid(auth.uid) ;
        }
        else
        {
            setIsLogged(false) ;
            setUid(undefined) ;
            setUsername(undefined) ;
        }
    }, [ auth ])

    useEffect( () => {
        if (uid !== undefined)
        {
            setUserDoc(doc(db, "Users", uid)) ;
        }
    }, [ uid ])

    useEffect(() => {
        if (userDoc !== undefined)
        {
            getDoc(userDoc)
            .then( docSnap => {
                setUsername(docSnap.data().metadata.username) ;
            })
        }
    }, [ userDoc ])

    useEffect( () => {
        if (status === true)
        {
            handlePendingRequestsOpen() ;
        }
    }, [ status ])

    const [ pendingRequests, setPendingRequests ] = useState([]) ;
    const [ pendingMails, setPendingMails ] = useState([]) ;

    useEffect(() => {
        if (!isEmpty(auth) && isLoaded(auth))
        {
            setIsLogged(true) ;
            setUid(auth.uid) ;
        }
        else
        {
            setIsLogged(false) ;
            setUid(undefined) ;
        }
    }, [ auth ])

    useEffect( () => {

        if (uid !== undefined)
        {
            FillPendingRequests() ;
            FillPendingMails() ;
        }

    }, [ uid ])

    const [ dialogPendingRequestsOpen, setDialogPendingRequestsOpen ] = useState(false) ;
    const handlePendingRequestsOpen = () => {
        setDialogPendingRequestsOpen(true) ;
    }
    const handlePendingRequestsClose = () => {
        onClose(false) ;
        setDialogPendingRequestsOpen(false) ;
    }

    const FillPendingRequests = () => {

        getDocs(query(collection(db, 'Users/' + uid + '/Mailbox'), where('type', '==', 'joinrequest')))
        .then( docs => {
            let _requests = [] ;
            docs.forEach( ( item, index) => {
                _requests.push(
                    {
                        id : item.id,
                        data : item.data()
                    }
                )
            })
            setPendingRequests(_requests) ;
        })
    }

    const FillPendingMails = () => {

        getDocs(query(collection(db, 'Users/' + uid + '/Mailbox'), where('type', '==', 'message')))
        .then( docs => {
            let _mails = [] ;
            docs.forEach( ( item, index) => {
                _mails.push(
                    {
                        id : item.id,
                        data : item.data()
                    }
                )
            })
            setPendingMails(_mails) ;
        })
    }

    const AcceptPendingRequest = ( request, user, tribe ) => {

        getDoc(doc(db, 'Dicts', 'Tribes'))
        .then( dictTribedoc => {
            let tribes = dictTribedoc.data().uids ;
            if (!tribes.includes(tribe.uid))
            {
                console.log("This tribe does not exist anymore") ;
                deleteDoc(doc(db, 'Users/' + uid + '/Mailbox', request)) ;
                FillPendingRequests() ;
            }
            else
            {
                getDoc(doc(db, 'Dicts', 'Users'))
                .then( dictUserdoc => {
                    let users = dictUserdoc.data().uids ;
                    if (!users.includes(user.uid))
                    {
                        console.log("This user does not exist anymore") ;
                        deleteDoc(doc(db, 'Users/' + uid + '/Mailbox', request)) ;
                        FillPendingRequests() ;
                    }
                    else
                    {
                        getDoc(doc(db, 'Tribes', tribe.uid))
                        .then( tribeDoc => {
                            let members = tribeDoc.data().members ;
                            let alreadyAMember = false ;
                            members.forEach( item => {
                                if (item.uid === user.uid)
                                {
                                    alreadyAMember = true ;
                                    deleteDoc(doc(db, 'Users/' + uid + '/Mailbox', request)) ;
                                    FillPendingRequests() ;
                                }
                            })
                            if (alreadyAMember)
                            {
                                console.log("The requester is already a member") ;
                            }      
                            else
                            {

                                updateDoc(doc(db, 'Tribes', tribe.uid), {
                                    members : arrayUnion({
                                        uid : user.uid,
                                        username : user.name,
                                    })
                                })
                                .then( () => {
                                    updateDoc(doc(db, 'Users', user.uid), {
                                        tribesasmember : arrayUnion({
                                            name : tribe.name,
                                            uid : tribe.uid,
                                        }),
                                        currentpendingrequests : arrayRemove({
                                            uid : tribe.uid,
                                            name : tribe.name
                                        })
                                    })
                                    .then( () => {
                                        deleteDoc(doc(db, 'Users/' + uid + '/Mailbox', request)) ;
                                        FillPendingRequests() ;
                                        addDoc(collection(db, "Users/" + user.uid + "/Mailbox"), {
                                            type : "message",
                                            timestamp : new Date(),
                                            content : "Your request to join the tribe " + tribe.name + " has been accepted."
                                        })    
                                    })
                                })
                            }
                        })            
                    }
                })
            }
        })
    }

    const DeclinePendingRequest = ( request, user, tribe ) => {
        deleteDoc(doc(db, 'Users/' + uid + '/Mailbox', request)) ;
        FillPendingRequests() ;
        addDoc(collection(db, "Users/" + user.uid + "/Mailbox"), {
            type : "message",
            timestamp : new Date(),
            content : "Your request to join the tribe " + tribe.name + " has been declined :("
        })
        .then( () => {
            updateDoc(collection(db, "Users/" + user.uid), {
                currentpendingrequests : arrayRemove({
                    uid : tribe.uid,
                    name : tribe.name
                })
            }) ;
        })
    }


    return (
        <div>
            <Dialog scroll = 'paper' sx = {{ maxHeight : 800, }} open = { dialogPendingRequestsOpen } onClose = { handlePendingRequestsClose }>
                <DialogTitle>Pending requests</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please manage your current pending requests
                    </DialogContentText>
                    <List sx = {{ width: '100%', maxWidth: 360, bgcolor: 'background.paper', borderRadius : 5 }}>
                        {
                            pendingRequests.map ( (item, index) => {
                                console.log(item)
                                return (
                                    <PendingRequestItem
                                        key = {'pr_' + index}
                                        username = { item.data.user.name }
                                        tribe = { item.data.tribe.name }
                                        onAccept = { () => AcceptPendingRequest(item.id, item.data.user, item.data.tribe) }
                                        onDecline = { () => DeclinePendingRequest(item.id, item.data.user, item.data.tribe) }
                                    />
                                )
                            })
                        }
                    </List>
                </DialogContent>
                <DialogActions>
                    <Button onClick = { handlePendingRequestsClose }>Cancel</Button>
                </DialogActions>
            </Dialog>

            { children }
        </div>
    )
}

export default PendingRequestsDialog ;