import { useState, useEffect } from 'react' ;
import { Typography, TextField, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material' ;
import { useFirebase } from 'react-redux-firebase' ;

import ResetPasswordDialog from './resetpasswordialog.js' ;

const LoginDialog = ( { status, onClose, children } ) => {

    const firebase = useFirebase() ;

    const [ email, setEmail ] = useState('') ;
    const [ emailError, setEmailError ] = useState(false) ;
    const [ password, setPassword ] = useState('') ;
    const [ passwordError, setPasswordError ] = useState(false) ;
    const [ errorMessage, setErrorMessage ] = useState('') ;

    const [ dialogLoginOpen, setDialogLoginOpen ] = useState(false) ;
    const handleLoginOpen = () => {
        setDialogLoginOpen(true) ;
    }
    const handleLoginClose = () => {
        setDialogLoginOpen(false) ;
        onClose(false) ;
    }

    const [ isResetPasswordDialogOpen, setIsResetPasswordDialogOpen ] = useState(false) ;
    const handleResetPasswordDialogChange = value => {
        setIsResetPasswordDialogOpen(value) ;
    }

    useEffect( () => {
        if (status === true)
        {
            handleLoginOpen() ;
        }
    }, [ status ])

    const handleFieldChange = (event) => {
        if (event.target.name === 'email')
        {
            setEmailError(false) ;
            setEmail(event.target.value) ;
        }
        else if (event.target.name === 'password')
        {
            setPasswordError(false) ;
            setPassword(event.target.value) ;
        }
    }

    const handleSubmitLogin = (event) => {
        event.preventDefault();
        setErrorMessage('') ;
        if (email === '') {
          setEmailError(true) ;
        }
        if (password === '') {
          setPasswordError(true) ;
        }
        if (password !== '' && email !== '') {
            firebase.auth().signInWithEmailAndPassword(email, password)
            .then( () => {
                handleLoginClose() ;
            })
            .catch((error) => {
            setErrorMessage(error.message);
          }) ;
        }
    }

    return (
        <div>
            <Dialog open = { dialogLoginOpen } onClose = { handleLoginClose }>
                <DialogTitle>Login</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        To log into your account, please enter your email address and your password here.
                    </DialogContentText>
                    <TextField
                        margin = "dense"
                        id = "name"
                        name = "email"
                        label = "Email Address"
                        type = "email"
                        fullWidth
                        required
                        value = { email }
                        onChange = { handleFieldChange }
                        autoComplete = "email"
                        variant = "standard"
                        error = { emailError }
                    />
                    <TextField
                        margin = "dense"
                        id = "password"
                        name = "password"
                        label = "Password"
                        type = "password"
                        secured = "true"
                        fullWidth
                        required
                        value = { password }
                        onChange = { handleFieldChange }
                        autoComplete = "password"
                        variant = "standard"
                        error = { passwordError }
                    />
                    {
                        errorMessage ? (
                            <Typography color = "error">{errorMessage}</Typography>
                        ) : null
                    }
                </DialogContent>
                <DialogActions>
                    <ResetPasswordDialog status = { isResetPasswordDialogOpen } onClose = { () => handleResetPasswordDialogChange(false) }>
                        <Button variant = "text" onClick = { () => { handleResetPasswordDialogChange(true) ; }}>Request a new password</Button>
                    </ResetPasswordDialog>
                    <Button onClick = { handleLoginClose }>Cancel</Button>
                    <Button autoFocus onClick = { handleSubmitLogin }>Login</Button>
                </DialogActions>
            </Dialog>

            { children }
        </div>
    )
}


export default LoginDialog ;
