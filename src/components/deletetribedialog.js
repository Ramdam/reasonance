import { useState, useEffect } from 'react' ;
import { Typography, FormControl, Select, InputLabel, MenuItem, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material' ;
import { getFirestore, collection, doc, addDoc, setDoc, updateDoc, getDoc, query, where, limit, getDocs, onSnapshot, serverTimestamp, arrayUnion, arrayRemove, increment, deleteDoc } from "firebase/firestore" ;
import { useFirebase, isEmpty, isLoaded } from 'react-redux-firebase' ;
import { useSelector } from "react-redux" ;


const DeleteTribeDialog = ( { status, onClose, children } ) => {

    const db = getFirestore() ;
    const auth = useSelector(state => state.firebase.auth) ;
    const [ isLogged, setIsLogged ] = useState(false) ;

    const [ uid, setUid ] = useState(undefined) ;
    const [ userDoc, setUserDoc ] = useState(undefined) ;
    const [ username, setUsername ] = useState(undefined) ;

    const [ tribesAsAdmin, setTribesAsAdmin ] = useState([])

    const [ selectedTribeToDeleteIndex, setSelectedTribeToDeleteIndex ] = useState(0) ;
    const handleSelectedTribeToDeleteChange = ( event ) => {
        if (tribesAsAdmin.length > 0)
            setSelectedTribeToDeleteIndex(event.target.value) ;
    }

    useEffect(() => {
        if (!isEmpty(auth) && isLoaded(auth))
        {
            setIsLogged(true) ;
            setUid(auth.uid) ;
        }
        else
        {
            setIsLogged(false) ;
            setUid(undefined) ;
            setUsername(undefined) ;
        }
    }, [ auth ])

    useEffect( () => {
        if (uid !== undefined)
        {
            setUserDoc(doc(db, "Users", uid)) ;
        }
    }, [ uid ])

    useEffect(() => {
        if (userDoc !== undefined)
        {
            getDoc(userDoc)
            .then( docSnap => {
                setUsername(docSnap.data().metadata.username) ;
                if (docSnap.data().tribesasadmin !== undefined)
                    setTribesAsAdmin(docSnap.data().tribesasadmin) ;
            })
        }
    }, [ userDoc ])

    useEffect( () => {
        if (status === true)
        {
            handleDeleteTribeOpen() ;
            if (userDoc !== undefined)
            {
                getDoc(userDoc)
                .then( docSnap => {
                    setUsername(docSnap.data().metadata.username) ;
                    if (docSnap.data().tribesasadmin !== undefined)
                        setTribesAsAdmin(docSnap.data().tribesasadmin) ;
                })
            }
        }
    }, [ status ])

    const [ dialogDeleteTribeOpen, setDialogDeleteTribeOpen ] = useState(false) ;
    const handleDeleteTribeOpen = () => {
        setDialogDeleteTribeOpen(true) ;
    }
    const handleDeleteTribeClose = () => {
        onClose(false) ;
        setDialogDeleteTribeOpen(false) ;
        setTribeToDeleteErrorMessage("") ;
    }
 
    const [ tribeToDeleteErrorMessage, setTribeToDeleteErrorMessage ] = useState("") ;
    const handleSubmitTribeToDelete = () => {

        let tribeToDelete = tribesAsAdmin[selectedTribeToDeleteIndex] ;
        console.log(tribeToDelete) ;
        let members = [] ;

        getDoc(doc(db, 'Tribes', tribeToDelete.uid))
        .then( docTribeToDelete => {

            members = docTribeToDelete.data().members ;

            members.forEach( member => {
                updateDoc(doc(db, 'Users', member.uid), {
                    tribesasmember : arrayRemove(tribeToDelete),
                    tribesasadmin : arrayRemove(tribeToDelete)
                })
                .then( () => {
                    addDoc(collection(db, "Users/" + member.uid + "/Mailbox"), {
                        type : "message",
                        timestamp : new Date(),
                        content : 'The tribe ' + tribeToDelete.name + ' has been deleted'
                    })
                })
            })

            deleteDoc(doc(db, 'Tribes', tribeToDelete.uid))
            .then( () => {
                console.log("Trying to update") ;
                updateDoc(doc(db, 'Dicts', 'Tribes'), {
                    names : arrayRemove(tribeToDelete.name),
                    uids : arrayRemove(tribeToDelete.uid),
                    tribes : arrayRemove(tribeToDelete)
                })
                .then( () => {
                    handleDeleteTribeClose() ;
                })
                .catch( error => {
                    console.log("Error : " + error) ;
                })
            })
        }) ;
    }


    return (
        <div>
            <Dialog open = { dialogDeleteTribeOpen } onClose = { handleDeleteTribeClose }>
                <DialogTitle>Delete an existing tribe.</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please select the tribe you want to delete.
                    </DialogContentText>
                    <FormControl variant = "standard" sx = {{ m: 1, minWidth: 120 }}>
                        <InputLabel id = "select-tribes">Tribes</InputLabel>
                        <Select
                            labelId = "select-tribes"
                            value = { selectedTribeToDeleteIndex }
                            onChange = { handleSelectedTribeToDeleteChange }
                            label = "Select your tribe"
                        >
                            {
                                tribesAsAdmin.map( (item, index ) => {
                                    return(
                                        <MenuItem key = { "tribe-" + index } value = { index }>
                                            <Typography>{item.name}</Typography>
                                        </MenuItem>
                                    )
                                })
                            }
                        </Select>
                    </FormControl>
                    {
                        tribeToDeleteErrorMessage ? (
                            <Typography color = "error">{tribeToDeleteErrorMessage}</Typography>
                        ) : null
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick = { handleDeleteTribeClose }>Cancel</Button>
                    <Button autoFocus onClick = { handleSubmitTribeToDelete }>Delete</Button>
                </DialogActions>
            </Dialog>
            { children }
        </div>
    )
}

export default DeleteTribeDialog ;