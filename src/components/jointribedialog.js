import { useState, useEffect } from 'react' ;
import { Typography, TextField, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material' ;
import { getFirestore, collection, doc, addDoc, setDoc, updateDoc, getDoc, query, where, limit, getDocs, onSnapshot, serverTimestamp, arrayUnion, arrayRemove, increment, deleteDoc } from "firebase/firestore" ;
import { useFirebase, isEmpty, isLoaded } from 'react-redux-firebase' ;
import { useSelector } from "react-redux" ;


const JoinTribeDialog = ( { status, onClose, children } ) => {

    const db = getFirestore() ;
    const auth = useSelector(state => state.firebase.auth) ;

    const [ uid, setUid ] = useState(undefined) ;
    const [ userDoc, setUserDoc ] = useState(undefined) ;
    const [ username, setUsername ] = useState(undefined) ;
    const [ tribesAsMember, setTribesAsMember ] = useState([]) ;
    const [ currentPendingRequests, setCurrentPendingRequests ] = useState([]) ;

    useEffect(() => {
        if (!isEmpty(auth) && isLoaded(auth))
        {
            setUid(auth.uid) ;
        }
        else
        {
            setUid(undefined) ;
            setUsername(undefined) ;
        }
    }, [ auth ])

    useEffect( () => {
        if (uid !== undefined)
        {
            setUserDoc(doc(db, "Users", uid)) ;
        }
    }, [ uid ])

    useEffect(() => {
        if (userDoc !== undefined)
        {
            UpdateContainers() ;
        }
    }, [ userDoc ])

    const UpdateContainers = () => {
        getDoc(userDoc)
            .then( docSnap => {
                if (docSnap.data().metadata !== undefined && docSnap.data().metadata.username !== undefined)
                    setUsername(docSnap.data().metadata.username) ;
                if (docSnap.data().currentpendingrequests !== undefined)
                    setCurrentPendingRequests(docSnap.data().currentpendingrequests) ;
                if (docSnap.data().tribesasmember !== undefined)
                    setTribesAsMember(docSnap.data().tribesasmember) ;
            })
    }

    useEffect( () => {
        if (status === true)
        {
            handleJoinTribeOpen() ;
        }
    }, [ status ])

    const [ dialogJoinTribeOpen, setDialogJoinTribeOpen ] = useState(false) ;
    const handleJoinTribeOpen = () => {
        setDialogJoinTribeOpen(true) ;
    }
    const handleJoinTribeClose = () => {
        onClose(false) ;
        setDialogJoinTribeOpen(false) ;
        setTribeNameToJoinErrorMessage("") ;
        setTribeNameToJoin("") ;
    }
    const [ tribeNameToJoin, setTribeNameToJoin ] = useState("") ;
    const handleFieldTribeNameToJoinChange = ( event ) => {
        setTribeNameToJoin(event.target.value) ;
    }
    const [ tribeNameToJoinError, setTribeNameToJoinError ] = useState(false) ;
    const [ tribeNameToJoinErrorMessage, setTribeNameToJoinErrorMessage ] = useState("") ;
    const handleSubmitTribeToJoin = () => {

        const tribeDict = doc(db, "Dicts", "Tribes") ;
        getDoc(tribeDict)
        .then(docSnap => {
            if (!docSnap.exists())
            {
                setTribeNameToJoinErrorMessage("This tribe does not exist. Please try another tribe name.") ;
            }
            else
            {
                if (docSnap.data().names !== undefined && !docSnap.data().names.includes(tribeNameToJoin))
                {
                    setTribeNameToJoinErrorMessage("This tribe does not exists. Please try another tribe name.") ;
                }
                else
                {
                    JoinTribe(tribeNameToJoin) ;
                }
            }
        })
    }

    const JoinTribe = async ( tribeName ) => {

        let adminUid = "" ;
        let tribeUid = "" ;
        let q = query(collection(db, "Tribes"), where("tribename", "==", tribeName), limit(1))
        getDocs(q)
        .then( querySnapshot => {
            querySnapshot.forEach((doc) => {
                adminUid = doc.data().admin.uid ;
                tribeUid = doc.id ;
            })

            let isAlreadyMember = false ;
            tribesAsMember.forEach( item => {
                if (item.uid === tribeUid)
                    isAlreadyMember = true ;
            })
            if (isAlreadyMember === true)
            {
                setTribeNameToJoinErrorMessage("You already are a member of this tribe.") ;
                return ;
            }
            let isAlreadyPending = false ;
            currentPendingRequests.forEach( item => {
                if (item.uid === tribeUid)
                    isAlreadyPending = true ;
            })
            if (isAlreadyPending === true)
            {
                setTribeNameToJoinErrorMessage("You already have a pending request to join this tribe.") ;
                return ;
            }
            getDoc(doc(db, "Users", adminUid))
            .then ( adminDoc => {
                addDoc(collection(db, "Users/" + adminUid + "/Mailbox"), {
                    type : "joinrequest",
                    timestamp : new Date(),
                    user : {
                        uid : uid,
                        name : username,    
                    },
                    tribe : {
                        name : tribeName,
                        uid : tribeUid,    
                    }
                })
                .then( () => {
                    updateDoc(userDoc, {
                        currentpendingrequests  : arrayUnion({
                            name : tribeName,
                            uid : tribeUid
                        })
                    })
                    .then( () => {
                        handleJoinTribeClose() ;
                    })
                })
            })
        })
    }

    return (
        <div>
            <Dialog open = { dialogJoinTribeOpen } onClose = { handleJoinTribeClose }>
                <DialogTitle>Join an existing tribe.</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please enter the name of the tribe. A request will be sent to the admin of the tribe.
                    </DialogContentText>
                    <TextField
                        margin = "dense"
                        id = "tribenametojoin"
                        name = "tribenametojoin"
                        label = "The tribe name"
                        type = "username"
                        fullWidth
                        required
                        value = { tribeNameToJoin }
                        onChange = { handleFieldTribeNameToJoinChange }
                        variant = "standard"
                        error = { tribeNameToJoinError }
                    />
                    {
                        tribeNameToJoinErrorMessage ? (
                            <Typography color = "error">{tribeNameToJoinErrorMessage}</Typography>
                        ) : null
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick = { handleJoinTribeClose }>Cancel</Button>
                    <Button autoFocus onClick = { handleSubmitTribeToJoin }>Send your request</Button>
                </DialogActions>
            </Dialog>

            { children }
        </div>
    )
}

export default JoinTribeDialog ;