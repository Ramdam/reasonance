import { useState, useEffect } from 'react' ;
import { Typography, FormControl, Select, InputLabel, MenuItem, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material' ;
import { getFirestore, collection, doc, addDoc, setDoc, updateDoc, getDoc, deleteDoc, query, where, limit, getDocs, onSnapshot, serverTimestamp, arrayUnion, arrayRemove, increment, LeaveDoc } from "firebase/firestore" ;
import { useFirebase, isEmpty, isLoaded } from 'react-redux-firebase' ;
import { useSelector } from "react-redux" ;
import DeleteTribeDialog from './deletetribedialog';

const LeaveTribeDialog = ( { status, onClose, children } ) => {

    const db = getFirestore() ;
    const auth = useSelector(state => state.firebase.auth) ;
    const [ isLogged, setIsLogged ] = useState(false) ;

    const [ uid, setUid ] = useState(undefined) ;
    const [ userDoc, setUserDoc ] = useState(undefined) ;
    const [ username, setUsername ] = useState(undefined) ;

    const [ tribesAsMember, setTribesAsMember ] = useState([])
    const [ tribesAsAdmin, setTribesAsAdmin ] = useState([])

    const [ selectedTribeToLeaveIndex, setSelectedTribeToLeaveIndex ] = useState(0) ;
    const handleSelectedTribeToLeaveChange = ( event ) => {
        if (tribesAsMember.length > 0)
            setSelectedTribeToLeaveIndex(event.target.value) ;
    }

    useEffect(() => {
        if (!isEmpty(auth) && isLoaded(auth))
        {
            setIsLogged(true) ;
            setUid(auth.uid) ;
        }
        else
        {
            setIsLogged(false) ;
            setUid(undefined) ;
            setUsername(undefined) ;
        }
    }, [ auth ])

    useEffect( () => {
        if (uid !== undefined)
        {
            setUserDoc(doc(db, "Users", uid)) ;
        }
    }, [ uid ])

    useEffect(() => {
        if (userDoc !== undefined)
        {
            getDoc(userDoc)
            .then( docSnap => {
                setUsername(docSnap.data().metadata.username) ;
                if (docSnap.data().tribesasadmin !== undefined)
                    setTribesAsAdmin(docSnap.data().tribesasadmin) ;
                if (docSnap.data().tribesasmember !== undefined)
                    setTribesAsMember(docSnap.data().tribesasmember) ;
            })
        }
    }, [ userDoc ])

    useEffect( () => {
        if (status === true)
        {
            handleLeaveTribeOpen() ;
            if (userDoc !== undefined)
            {
                getDoc(userDoc)
                .then( docSnap => {
                    setUsername(docSnap.data().metadata.username) ;
                    if (docSnap.data().tribesasadmin !== undefined)
                        setTribesAsAdmin(docSnap.data().tribesasadmin) ;
                    if (docSnap.data().tribesasmember !== undefined)
                        setTribesAsMember(docSnap.data().tribesasmember) ;
                })
            }
        }
    }, [ status ])

    const [ dialogLeaveTribeOpen, setDialogLeaveTribeOpen ] = useState(false) ;
    const handleLeaveTribeOpen = () => {
        setDialogLeaveTribeOpen(true) ;
    }
    const handleLeaveTribeClose = () => {
        onClose(false) ;
        setDialogLeaveTribeOpen(false) ;
        setTribeToLeaveErrorMessage("") ;
    }
 
    const [ tribeToLeaveErrorMessage, setTribeToLeaveErrorMessage ] = useState("") ;
    const handleSubmitTribeToLeave = () => {

        let tribeToLeave = tribesAsMember[selectedTribeToLeaveIndex] ;
        if (tribesAsMember.includes(tribeToLeave))
        {
            leaveAsAdmin() ;
        }
        else
        {
            leaveAsMember() ;
        }
    }

    const deleteTribe = () => {

        let tribeToDelete = tribesAsMember[selectedTribeToLeaveIndex] ;

        getDoc(doc(db, 'Tribes', tribeToDelete.uid))
        .then( docTribeToDelete => {

            deleteDoc(doc(db, 'Tribes', tribeToDelete.uid))
            .then( () => {
                console.log("Trying to update") ;
                updateDoc(doc(db, 'Dicts', 'Tribes'), {
                    names : arrayRemove(tribeToDelete.name),
                    uids : arrayRemove(tribeToDelete.uid),
                    tribes : arrayRemove(tribeToDelete)
                })
                .then( () => {
                    updateDoc(doc(db, 'Users', uid), {
                        tribesasmember : arrayRemove(tribeToDelete),
                        tribesasadmin : arrayRemove(tribeToDelete)
                    })
                    .then( () => {
                        let members = [] ;
                        getDoc(doc(db, 'Tribes', tribeToDelete.uid))
                        .then( docTribeToDelete => {
                            members = docTribeToDelete.data().members ;
                            members.forEach( member => {
                                updateDoc(doc(db, 'Users', member.uid), {
                                    tribesasmember : arrayRemove(tribeToDelete),
                                    tribesasadmin : arrayRemove(tribeToDelete)
                                })
                                .then( () => {
                                    addDoc(collection(db, "Users/" + member.uid + "/Mailbox"), {
                                        type : "message",
                                        timestamp : new Date(),
                                        content : 'The tribe ' + tribeToDelete.name + ' has been deleted'
                                    })
                                })
                                .then( () => {
                                    handleLeaveTribeClose() ;
                                })
                            })
                        })
                    })
                })
                .catch( error => {
                    console.log("Error : " + error) ;
                })
            })
        }) ;

    }

    const leaveAsAdmin = () => {

        let tribeToLeave = tribesAsMember[selectedTribeToLeaveIndex] ;

        getDoc(doc(db, 'Tribes', tribeToLeave.uid))
        .then( docTribeToLeave => {

            let members = docTribeToLeave.data().members ;
            if (members.length <= 1)
            {
                deleteTribe() ;
            }
            else
            {
                let admin = docTribeToLeave.data().admin ;
                let newAdmin = members[1] ;
                
                updateDoc(doc(db, 'Tribes', tribeToLeave.uid), {
                    members : arrayRemove({
                        uid : uid,
                        username : username,
                    }),
                    admin : newAdmin,
                })
                .then( () => {
                    updateDoc(doc(db, 'Users', uid), {
                        tribesasmember : arrayRemove({
                            uid : tribeToLeave.uid,
                            username : tribeToLeave.name,
                        }),
                        tribesasadmin : arrayRemove({
                            uid : tribeToLeave.uid,
                            username : tribeToLeave.name,
                        })
                    })
                    .then( () => {
                        addDoc(collection(db, "Users/" + newAdmin.uid + "/Mailbox"), {
                            type : "message",
                            timestamp : new Date(),
                            content : 'The admin of the tribe ' + tribeToLeave.name +  ' has left. You were designated as the new admin.'
                        })
                        .then( () => {
                            handleLeaveTribeClose() ;
                        })        
                    })
                })   
            }
        }) ;
    }
    
    const leaveAsMember = () => {

        let tribeToLeave = tribesAsMember[selectedTribeToLeaveIndex] ;

        getDoc(doc(db, 'Tribes', tribeToLeave.uid))
        .then( docTribeToLeave => {

            let admin = docTribeToLeave.data().admin ;
            addDoc(collection(db, "Users/" + admin.uid + "/Mailbox"), {
                type : "message",
                timestamp : new Date(),
                content : username + ' has leaved the tribe ' + tribeToLeave.name
            })
            .then(() => {
                updateDoc(doc(db, 'Tribes', tribeToLeave.uid), {
                    members : arrayRemove({
                        uid : uid,
                        username : username,
                    })
                })
                .then( () => {
                    updateDoc(doc(db, 'Users', uid), {
                        tribesasmember : arrayRemove({
                            uid : tribeToLeave.uid,
                            username : tribeToLeave.name,
                        })
                    })
                    .then( () => {
                        handleLeaveTribeClose() ;
                    })    
                })
            })
        }) ;
    }

    
    return (
        <div>
            <Dialog open = { dialogLeaveTribeOpen } onClose = { handleLeaveTribeClose }>
                <DialogTitle>Leave an existing tribe.</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please select the tribe you want to leave.
                    </DialogContentText>
                    <FormControl variant = "standard" sx = {{ m: 1, minWidth: 120 }}>
                        <InputLabel id = "select-tribes">Tribes</InputLabel>
                        <Select
                            labelId = "select-tribes"
                            value = { selectedTribeToLeaveIndex }
                            onChange = { handleSelectedTribeToLeaveChange }
                            label = "Select your tribe"
                        >
                            {
                                tribesAsMember.map( (item, index ) => {
                                    return(
                                        <MenuItem key = { "tribe-" + index } value = { index }>
                                            <Typography>{item.name}</Typography>
                                        </MenuItem>
                                    )
                                })
                            }
                        </Select>
                    </FormControl>
                    {
                        tribeToLeaveErrorMessage ? (
                            <Typography color = "error">{tribeToLeaveErrorMessage}</Typography>
                        ) : null
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick = { handleLeaveTribeClose }>Cancel</Button>
                    <Button autoFocus onClick = { handleSubmitTribeToLeave }>Leave</Button>
                </DialogActions>
            </Dialog>
            { children }
        </div>
    )
}

export default LeaveTribeDialog ;