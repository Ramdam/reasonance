import { useState, useEffect } from 'react' ;
import { Typography, TextField, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material' ;
import { useFirebase } from 'react-redux-firebase' ;

const ResetPasswordDialog = ( { status, onClose, children } ) => {

    const firebase = useFirebase() ;

    const [ email, setEmail ] = useState('') ;
    const [ emailError, setEmailError ] = useState(false) ;
    const [ errorMessage, setErrorMessage ] = useState('') ;

    const [ dialogResetPasswordOpen, setDialogResetPasswordOpen ] = useState(false) ;
    const handleResetPasswordOpen = () => {
        setDialogResetPasswordOpen(true) ;
    }
    const handleResetPasswordClose = () => {
        setDialogResetPasswordOpen(false) ;
        onClose(false) ;
    }

    useEffect( () => {
        if (status === true)
        {
            handleResetPasswordOpen() ;
        }
    }, [ status ])

    const handleFieldChange = (event) => {
        if (event.target.name === 'email')
        {
            setEmailError(false) ;
            setEmail(event.target.value) ;
        }
    }

    const handleSubmitResetPassword = (event) => {
        event.preventDefault();
        setErrorMessage('') ;
        if (email === '') {
          setEmailError(true) ;
        }
        if (email !== '') {
            firebase.auth().sendPasswordResetEmail(email)
            .then( () => {
                handleResetPasswordClose() ;
            })
            .catch((error) => {
                setErrorMessage(error.message);
            }) ;
        }
    }

    return (
        <div>
            <Dialog open = { dialogResetPasswordOpen } onClose = { handleResetPasswordClose }>
                <DialogTitle>Request a new password</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please enter your email address and check your inbox. We will send you a link to reset your password.
                    </DialogContentText>
                    <TextField
                        margin = "dense"
                        id = "name"
                        name = "email"
                        label = "Email Address"
                        type = "email"
                        fullWidth
                        required
                        value = { email }
                        onChange = { handleFieldChange }
                        autoComplete = "email"
                        variant = "standard"
                        error = { emailError }
                    />
                    {
                        errorMessage ? (
                            <Typography color = "error">{errorMessage}</Typography>
                        ) : null
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick = { handleResetPasswordClose }>Cancel</Button>
                    <Button autoFocus onClick = { handleSubmitResetPassword }>Send</Button>
                </DialogActions>
            </Dialog>
            { children }
        </div>
    )
}

export default ResetPasswordDialog ;
